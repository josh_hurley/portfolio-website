# Internship Portfolio
This website has been created to demonstrate some skills and technologies I have picked up during my 
studies as well as my internship.  It is intended to be a brief summary of my learnings, not a complete 
picture.  It also only uses basic styling and no external frameworks so that it may be easily used by 
an assessor if required.