function showClass(evt, classToShow, list) {
    var x = document.getElementsByClassName(list);
    for (var i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }

    var tabitems = document.getElementsByClassName("tab-item");
    if (list=='skill-list') {
      for (var i = 0; i < x.length; i++) {
        tabitems[i].style.backgroundColor = "slategrey";
      }
    }
    
      if (list=='project-list') {
      for (var i = 0; i < x.length; i++) {
        tabitems[i+2].style.backgroundColor = "slategrey";
      }
    }
    
    document.getElementById(classToShow).style.display = "block";
    evt.target.style.backgroundColor = "cadetblue";
}

window.onscroll = function() {scrollUpdate()};

function scrollUpdate() {
  var backToTopButton = document.getElementById("top-btn");
  
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    backToTopButton.style.display="inline-block";
  } else {
    backToTopButton.style.display="none";
  }
}

function scrollToTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}